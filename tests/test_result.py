import sqarf
from sqarf.qatest import QAResult


def test_result_status():
    failed = ("FAILED", "ERROR")
    passed = ("IRRELEVANT", "PASSED", "FIXED")
    na = ("NOT_RAN", "IRRELEVANT")

    for status_name in failed:
        status = getattr(QAResult.STATUS, status_name)
        r = QAResult(status, "testing FAIL result")
        assert r.failed()

    for status_name in passed:
        status = getattr(QAResult.STATUS, status_name)
        r = QAResult(status, "testing PASSED result")
        assert r.passed()

    for status_name in na:
        status = getattr(QAResult.STATUS, status_name)
        r = QAResult(status, "testing N/A result")
        assert r.not_applicable()


def test_result_sum():
    err = QAResult.ERROR("Test Err", Exception("Test Err"), "Test Trace")
    skipped = QAResult.SKIPPED("Test Skipped")
    not_ran = QAResult.NOT_RAN()
    irr = QAResult.IRRELEVANT("Test irrelevant")
    passed = QAResult.PASSED("Test Passed")
    failed = QAResult.FAILED("Test Failed")
    fixed = QAResult.FIXED("Test Fixed")

    failing_lists = (
        (skipped, err),
        (passed, err),
        (not_ran, err),
        (irr, err),
        (failed, err),
        (fixed, err),
        (skipped, failed),
        (passed, failed),
        (not_ran, failed),
        (irr, failed),
        (failed, failed),
        (fixed, failed),
    )
    for result_list in failing_lists:
        assert sum(result_list, passed).failed()

    passing_lists = (
        (passed, passed),
        (skipped, passed),
        (not_ran, passed),
        (irr, passed),
        (fixed, passed),
    )
    for result_list in passing_lists:
        assert sum(result_list, passed).passed()

    not_applicable_lists = (
        (skipped, skipped),
        (skipped, not_ran),
        (skipped, irr),
        (not_ran, not_ran),
        (not_ran, irr),
        (irr, irr),
    )
    for result_list in not_applicable_lists:
        assert sum(result_list, irr).not_applicable()
