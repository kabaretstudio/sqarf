import os
import pytest

from sqarf import QATest, Session
import sqarf.html_table_export


def number_tests():
    """
    Flake8 required to lower `text_tests` complexity :|
    """

    class IntOnly(QATest):
        def relevant_for(self, context):
            try:
                float(context["my_stuff"])
            except ValueError:
                return False, "This value is not even a number."
            return True, "The value sounds good for us, let's check it."

    class FloatOnly(QATest):
        def relevant_for(self, context):
            try:
                float(context["my_stuff"])
            except ValueError:
                return False, "This value is not even a number."
            return True, "The value sounds good for us, let's check it."

    class NumbersTests(QATest):
        """
        A group of tests for numbery things.
        """

        def get_sub_test_types(self):
            return (IntOnly, FloatOnly)

    return NumbersTests


@pytest.fixture
def text_tests():
    """
    This shows how to create hierarchical test classes.
    """

    import string

    letters = list(string.ascii_lowercase)
    vowels = "aeiouy"
    consonants = [c for c in letters if c not in vowels]

    class MyTestType(QATest):
        """Base class for those tests."""

        LETTERS = letters
        VOWELS = vowels
        CONSONANTS = consonants

    class HasVowel(MyTestType):
        """Ensure a vowel is present."""

        def test(self, context):
            with self.log.section("This is a log section") as log:
                my_stuff = context["my_stuff"].lower()
                for vowel in self.VOWELS:
                    log.info("Testing", vowel)
                    if vowel in my_stuff:
                        log.info("  Found.")
                        return True, "found " + vowel
                log.info("No vowelr Found :(")
                return False, "No vowel found"

    class HasAllVowels(MyTestType):
        """Ensure all vowels are used."""

        def can_fix(self, context):
            my_stuff = context["my_stuff"].lower()
            if str(my_stuff) == my_stuff:
                return True, "We'll just append missing one..."
            else:
                return False, "This is not even like a string :/"

        def fix(self, context):
            my_stuff = str(context["my_stuff"])
            lowered = my_stuff.lower()
            with self.log.section("Fixing", repr(my_stuff)) as log:
                for vowel in self.VOWELS:
                    if vowel not in lowered:
                        log.info("adding", vowel)
                        my_stuff += vowel
                log.info("Updating context with fixed data:", my_stuff)
                context["my_stuff"] = my_stuff
                return True, "All missing vowels added"

        def test(self, context):
            my_stuff = context["my_stuff"].lower()
            with self.log.section("Testing on", my_stuff) as log:
                for vowel in self.VOWELS:
                    log.info("Testing", vowel)
                    if vowel not in my_stuff:
                        log.info("  ! Missing !")
                        return False, "missing " + vowel
                log.info("All vowels found \\o/")
                return True, "All vowel found"

    class HasConsonant(MyTestType):
        """Ensure a consonant is present."""

        def test(self, context):
            my_stuff = context["my_stuff"].lower()
            for letter in my_stuff:
                if letter not in self.VOWELS:
                    return True, "found " + letter
            return False, "No consonant found !"

    class HasAllConsonants(MyTestType):
        """
        Ensure all consonants are used.

        Because yes we want to do this and also
        we want to have a long description here.
        """

        def test(self, context):
            my_stuff = context["my_stuff"].lower()
            for consonant in self.CONSONANTS:
                if consonant not in my_stuff:
                    return False, "missing " + consonant
            return True, "All consonant found"

    class ConsonanceTests(QATest):
        """
        This test acts as a group for a few other tests.
        """

        def get_sub_test_types(self):
            return (
                HasVowel,
                HasAllVowels,
                HasConsonant,
                HasAllConsonants,
            )

    class LettersOnly(MyTestType):
        def test(self, context):
            my_stuff = context["my_stuff"].lower()
            for c in my_stuff:
                if c not in self.LETTERS:
                    return False, 'Got "{}"'.format(c)
            return True, ""

    class UppersOnly(MyTestType):
        def test(self, context):
            my_stuff = context["my_stuff"]
            # raise an error, for tests:
            1 / 0
            if my_stuff.upper() == my_stuff:
                return True, ""
            else:
                return False, "Got some lower letters in there"

    class CharactersTests(QATest):
        """
        This test acts as a group for a few other tests.
        """

        def get_sub_test_types(self):
            return (
                LettersOnly,
                UppersOnly,
            )

    NumbersTests = number_tests()

    return [ConsonanceTests, CharactersTests, NumbersTests]


def test_usage(text_tests):
    """
    This test show how to run a collection of tests.
    """
    session = Session()
    session.register_test_types(text_tests)
    session.context_set(
        my_stuff="ABCDEFGHIJKLMNOPQRSTUVWxyz?",
    )
    result = session.run()
    print("\n".join(session.to_lines()))
    assert not result.passed()
