from sqarf import QATest


def test_test_description():
    class MyTest(QATest):
        """
        Short description.

        Long description here with
        several lines of text
        to test the thing.
        """

    short, long = MyTest.test_description()
    assert short == "Short description."
    assert long == (
        "Long description here with several " "lines of text to test the thing."
    )
