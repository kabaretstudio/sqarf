import os
import pytest

from sqarf import Session
from test_usage import text_tests

OPEN_IN_BROWSER = False


@pytest.fixture
def exportable_session(text_tests):     # noqa: F811 (redefinition of 'text_tests')

    session = Session()
    session.register_test_types(text_tests)
    session.set_title("TESTING EXPORTS")

    custom_test_file = os.path.normpath(
        os.path.join(__file__, "..", "_custom_tests.py")
    )
    session.register_tests_from_file(custom_test_file)

    import test_session

    session.register_tests_from_string(test_session.TEST_CODE, "custom_test_code")

    session.context_set(my_stuff="ABCDEFGHIJKLMNOPQRST-VWXYZ")
    session.run()
    session.run()
    session.run()

    return session


def test_html_tree_export(exportable_session, tmp_path):
    """
    This show how to export a html "tree view" of the
    test results.
    """

    filename = str(tmp_path / "sqarf_tree_report.html")
    print("Exporting to", filename)

    config = {}
    exporter = exportable_session.get_exporter("Html Tree", config)
    exporter.export(
        exportable_session.to_dict_list(),
        filename,
        allow_overwrite=False,
        also_open_in_browser=OPEN_IN_BROWSER,
    )


def test_html_table_export(exportable_session, tmp_path):
    """
    This show how to export a html "table view" of the
    test results (w/o javascript needed, suitable for
    QTextEdit widget)
    """
    filename = str(tmp_path / "sqarf_tree_report.html")
    print("Exporting to", filename)

    config = {}
    config["Show Logs"] = True
    config["Show Debug Log"] = False
    config["Show Times"] = True
    config["Show Descriptions"] = True
    config["Show Context Edits"] = True
    config["Show Context Data"] = True
    exporter = exportable_session.get_exporter("Html Table", config)
    exporter.export(
        exportable_session.to_dict_list(),
        filename,
        allow_overwrite=False,
        also_open_in_browser=OPEN_IN_BROWSER,
    )


def test_json_export(exportable_session, tmp_path):
    """
    This show how to export a json file of the
    test results.
    """
    filename = str(tmp_path / "sqarf_tree_report.html")
    print("Exporting to", filename)

    config = {}
    exporter = exportable_session.get_exporter("JSON", config)
    exporter.export(
        exportable_session.to_dict_list(),
        filename,
        allow_overwrite=False,
        also_open_in_browser=OPEN_IN_BROWSER,
    )
